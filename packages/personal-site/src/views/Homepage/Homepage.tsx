import React from 'react';

/**
 * Component imports
 */
import About from '../../components/About';
import Navbar from '../../components/Navbar';
import Home from '../../components/Home';
import Expertise from '../../components/Expertise';
import Projects from '../../components/Projects';

const Homepage = () => {

  // const handleOnDragEnd = (item: any) => {
  //   if (!item.destination) return;
  //   const items = Array.from(cardItems);
  //   const [reorderedItem] = items.splice(item.source.index, 1);
  //   items.splice(item.destination.index, 0, reorderedItem);

  //   setCardItems(items);
  // }

  return (
    <>
      <Navbar />
      <div className="homepage-container">
        <section id="home" className="section">
          <Home />
        </section>
        <section id="expertise" className="section">
          <Expertise />
        </section>
        <section id="about" className="section">
          <About />
        </section>
        <section id="projects" className="section">
          <Projects />
        </section>
      </div>
    </>
  )
};

export default Homepage;
