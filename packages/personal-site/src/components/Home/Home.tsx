import React from 'react';

const PUBLIC_URL = process.env.PUBLIC_URL;

const Home = () => {
  return (
    <div className="section-home">
      <div className="home-bg" style={{ backgroundImage: `url(${PUBLIC_URL + '/assets/background.png'})` }}></div>
      <div className="ball-container">
        <div className="ball"></div>
      </div>
      <div className="section-home-text">
        <h2>
          Jared Bourget
        </h2>
        <p>
          Software Engineer, Front End Developer.
        </p>
      </div>
    </div>
  )
}

export default Home;
