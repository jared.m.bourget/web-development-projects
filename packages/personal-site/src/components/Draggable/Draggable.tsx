import React from 'react';
import { DragDropContext, Droppable, Draggable as DndDraggable } from '@hello-pangea/dnd';

type DraggableItemType = Array<
  {
    id: string;
    text?: string;
    [keys: string]: any
  }
>

type DraggablePropTypes = {
  /**
   * Any additional classes passed to the draggable component (not being used currently)
   */
  classes?: string;

  /**
   * Function to fire when item is dropped
   */
  handleDropFunction: (item: any) => void;

  /**
   * Array dataset - needs an id
   */
  dataset: DraggableItemType;

  /**
   * Property to serve as Draggable key and id
   */
  itemId: string,

  /**
   * vertical or horizonal orientation (not being used currently)
   */
  variant?: 'v-list' | 'h-list'
}

/**
 * 
 * @param {Array} props.dataset Array of list items
 * @param {string} props.classes Additional classes
 * @param {Function} props.handleDropFunction Function to fire when item is dropped
 * @param {string} props.itemId Property to be used as key/id
 * @param {string} props.variant vertical or horizontal orientation - vertical by default
 * @returns 
 */
const Draggable = (props: DraggablePropTypes) => {
  const {
    dataset,
    handleDropFunction,
    itemId,
    variant = 'v-list',
  } = props;

  return (
    <DragDropContext onDragEnd={handleDropFunction}>
      <Droppable droppableId="cardItems">
        {(provided, snapshot) => (
          <div className={`droppable-container ${variant} ${snapshot.isDraggingOver ? 'dragOver' : ''}`} {...provided.droppableProps} ref={provided.innerRef}>
            {
              dataset.map((draggableItem, index) => {
                return (
                  <DndDraggable key={draggableItem[itemId]} draggableId={draggableItem[itemId]} index={index}>
                    {(provided, snapshot) => (
                      <div
                        className={`${snapshot.isDragging ? 'dragging' : ''}`}
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <p>
                          {draggableItem.text}
                        </p>
                      </div>
                    )}
                  </DndDraggable>
                )
              })
            }
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </DragDropContext>
  )
}

export default Draggable;
