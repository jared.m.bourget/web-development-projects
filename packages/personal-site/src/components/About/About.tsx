import React from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import { Tooltip } from 'react-tooltip';

import carouselCardData from '../../carouselCardData.json';

const PUBLIC_PATH = process.env.PUBLIC_URL;

type AboutCardProps = {
  backgroundImg: string,
  backgroundAlt: string,
  centralImg: string,
  centralAlt: string,
  title: string,
  line1: string,
  line2: string,
}

const AboutCard = (props: AboutCardProps) => {
  const {
    backgroundImg,
    backgroundAlt,
    centralImg,
    centralAlt,
    title,
    line1,
    line2,
  } = props;

  return (
    <div className="section-about-card">
      <img className="background-img" src={`${PUBLIC_PATH + backgroundImg}`} alt={`${backgroundAlt}`}></img>
      <img className="central-img" src={`${PUBLIC_PATH + centralImg}`} alt={`${centralAlt}`}></img>
      <h3>{title}</h3>
      <p>{line1}</p>
      <p>{line2}</p>
    </div>
  );
};

const About = () => {

  return (
    <div className="section-about">
      <h2>About</h2>
      <div className="section-about-hobbies-wrapper">
        <div className="section-about-info-wrapper">
          <div className="section-about-text-wrapper">
            <h3>Who am I?</h3>
            <hr />
            <p>
              I am a full stack software engineer with a diverse background including manufacturing,
              chemistry, mathematics, and health care provision. On my journey to becoming a
              software engineer, I have learned Angular, React, Node, Express, PostgresQL, and
              MongoDB.
            </p>
            <p>
              I love coding because it offers a well-balanced blend of creativity and
              problem solving rarely seen in other disciplines. Coding provides an endless
              opportunity for intellectual growth and improvement - of which I will continue to take full
              advantage. I hope to integrate much of my background into future projects, especially
              applications concerning health.
            </p>
          </div>
        </div>
        <div className="section-about-carousel-wrapper">
          <div className="section-about-text-wrapper">
            <h3>Hobbies</h3>
            <hr />
          </div>
          <Carousel showThumbs={false} className="section-about-carousel">
            {
              carouselCardData.map((item, index) => {
                return (
                  <AboutCard key={index} {...item} />
                )
              })
            }
          </Carousel>
        </div>
      </div>

      <div className="section-about-footer-wrapper">
        <a href="https://www.gitlab.com">
          <img src={`${PUBLIC_PATH + "/assets/gitlab.svg"}`} alt="GitLab" />
        </a>
        <div className="icon-divider" />
        <a href="https://www.linkedin.com">
          <img src={`${PUBLIC_PATH + "/assets/linkedin.svg"}`} alt="LinkedIn" />
        </a>
        <div className="icon-divider" />
        <a data-tooltip-id="resume-tooltip" data-tooltip-content="Download Resume" className="button" href="./assets/Bourget_Resume.pdf" download="Bourget_Resume.pdf">
          <img src={`${PUBLIC_PATH + "/assets/file-text.svg"}`} alt="download resume" />
        </a>
      </div>
      <Tooltip
        id="resume-tooltip"
        data-tooltip-id="resume"
        content="Download Resume"
        place="top-end"
      />
    </div>
  )
};

export default About;
