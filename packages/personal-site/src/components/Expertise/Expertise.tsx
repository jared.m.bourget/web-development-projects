import React from 'react';
import Card from '../Card';

import cardInfo from '../../expertiseCardData.json';

const Expertise = () => {
  return (
    <div className="section-expertise">
      <h2>Expertise</h2>
      <div className="row expertise-container">
        {
          cardInfo.map((card) => {
            return <Card key={card.cardId} { ...card } />
          })
        }
      </div>
    </div>
  )
}

export default Expertise;
