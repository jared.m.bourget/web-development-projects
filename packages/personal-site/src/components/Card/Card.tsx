import React from 'react';

const PUBLIC_PATH = process.env.PUBLIC_URL;

interface CardPropTypes extends React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  text: string;
  title: string;
  imgURL: string;
  imgAlt: string;
  cardId?: string | number;
}

const Card: React.FC<CardPropTypes> = ({
  text,
  title,
  imgURL,
  imgAlt,
  ...props
}) => {
  return (
    <div className="card">
      <h3 className="card-header">
        <img src={`${PUBLIC_PATH + imgURL}`} alt={`${imgAlt}`} />
        <span>
          {title}
        </span>
      </h3>
      <hr />
      <div className="card-body">
        <p>
          {text}
        </p>
      </div>
    </div>
  )
};

export default Card;