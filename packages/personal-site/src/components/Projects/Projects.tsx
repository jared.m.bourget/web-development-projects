import React from 'react';

const PUBLIC_URL = process.env.PUBLIC_URL;

const Projects = () => {
  return (
    <div className="section-projects">
      <h2>Projects</h2>
      <div className="section-projects-wrapper">
        <iframe title="shopping-project" className="section-projects-shopping" loading="lazy" allowFullScreen src="https://www.jmbourget.com" />
        <div className="section-projects-shopping-desc">
          <h3>Sample Ecommerce Website</h3>
          <p>A simple ecommerce website that allows users to create an account and sign in, add/remove items to their cart, and checkout.</p>
          <p>Using Redux, items added to the cart are saved to both the application state and local storage - this allows the cart to persist upon page refresh.</p>
          <p>The application also showcases a light/dark mode toggle, which is also saved to local storage</p>
          <p>This project was developed using React, Redux, TypeScript, SCSS, Firebase, and Firestore</p>
          <a href="https://www.jmbourget.com">Project Link</a>
          <a href="https://gitlab.com/jaredbourget/udemy-capstone-react-2022">GitLab Link</a>
        </div>
      </div>
      <div className="section-projects-wrapper">
        <img src={`${PUBLIC_URL + "/assets/vetlink.png"}`} alt="vetlink" />
        <div className="section-projects-landing-desc">
          <h3>Sample Landing Page</h3>
          <p>This project was an exercise in responsive design using vanilla JS, Bootstrap, and CSS.</p>
          <p>Although is project is not hosted, the source code may be viewed here:
            <a href="https://gitlab.com/jaredbourget/web-development-projects/-/tree/main/packages/VetLink?ref_type=heads">GitLab Link</a>
          </p>
        </div>
      </div>
    </div>
  );
};

export default Projects;
