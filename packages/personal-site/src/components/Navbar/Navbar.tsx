import React, { useState } from 'react';
import { Link } from 'react-scroll';

import useWindowDimensions from '../../hooks';

const PUBLIC_PATH = process.env.PUBLIC_URL;

type NavbarMenuPropTypes = {
  menu?: boolean;
  open?: boolean;
};

const NavbarMenu = (props: NavbarMenuPropTypes) => {
  const { menu, open } = props;

  return (
    <ul className={`nav-list ${menu ? "menu" : ""} ${open ? "open" : ""}`} >
      <Link className="nav-list-item" isDynamic activeClass="active" smooth spy offset={-50} to="home">
        Home
      </Link>
      <Link className="nav-list-item" isDynamic activeClass="active" smooth spy offset={-40} to="expertise">
        Expertise
      </Link>
      <Link className="nav-list-item" isDynamic activeClass="active" smooth spy offset={-40} to="about">
        About
      </Link>
      <Link className="nav-list-item" isDynamic activeClass="active" smooth spy offset={-40} to="projects">
        Projects
      </Link>
    </ul>
  )
}

const Navbar = () => {
  const { width } = useWindowDimensions();
  const [isOpen, setIsOpen] = useState(false);

  const menu = width < 600;

  const toggleMenuIsOpen = () => {
    setIsOpen((prev) => !prev);
  }

  if (!menu && isOpen) {
    setIsOpen(() => false);
  }

  return (
    <nav className="nav-container">
      <h1>
        Jared Bourget
      </h1>
      {
        menu ? (
          <div className="nav-menu-drawer">
            <img src={`${PUBLIC_PATH + "/assets/menu.svg"}`} alt="Menu" onClick={() => toggleMenuIsOpen()} />
            <NavbarMenu open={isOpen} menu={true} />
          </div>
        ) : (
          <NavbarMenu />
        )
      }
    </nav>
  )
}

export default Navbar;