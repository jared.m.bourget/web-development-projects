import React from 'react';
import { HashRouter, Routes, Route } from "react-router-dom";

/**
 * View imports
*/
import Homepage from './views/Homepage';

function App() {
  return (
    <HashRouter basename="/">
      <Routes>
        <Route path="/" Component={Homepage} />
      </Routes>
    </HashRouter>
  );
}

export default App;
