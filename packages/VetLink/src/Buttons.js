
const updateMessageText = (event) => {
  const buttonText = event.target.innerText;
  const toastMessage = document.getElementsByClassName('toast-body');
  toastMessage[0].innerHTML = `${buttonText} currently has no functionality`
};

const showToast = (event) => {
  const toastElement = document.getElementById('liveToast');
  if (toastElement.style.display !== "block") toastElement.style.display = "block"
};

const hideToast = (event) => {
  const toastElement = document.getElementById('liveToast');
  toastElement.style.display = "none";
};

const download = (event) => {
  updateMessageText(event);
  showToast();
};

const contactUs = (event) => {
  updateMessageText(event);
  showToast();
};

const getStated = (event) => {
  updateMessageText(event);
  showToast();
};

const signUp = (event) => {
  updateMessageText(event);
  showToast();
};